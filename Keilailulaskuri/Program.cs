﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Keilailulaskuri
{
    class Program
    {
        static void Main(string[] args)
        {
            Aloitustekstit();
            PyoritaPelia();
        }

        static private void Aloitustekstit()
        {
            Console.WriteLine("Keilauksen pistelaskuri");
            Console.WriteLine("Hei, lasken keilauskierroksen pisteet!");
            Console.WriteLine("Muista merkitä kaadot aina X-kirjaimella ja");
            Console.WriteLine("paikot aina /-merkillä.");
            Console.WriteLine();
        }

        static private void PyoritaPelia()
        {
            KeilausLaskuri laskuri = new KeilausLaskuri();
            while (true)
            {
                while (true)
                {
                    string heitto;
                    Console.WriteLine("Syötä " + laskuri.nykyisenRuudunNumero() + ". ruudun " + laskuri.nykyisenHeitonNumeroRuudussa() + ". heitto:");
                    heitto = Console.ReadLine();
                    heitto = heitto.ToUpper();
                    laskuri.lisaaHeitto(heitto);
                    if (laskuri.getNykyinenHeitto() == 20 && heitto != "X" && heitto != "/")
                    {
                        laskuri.kivaTulostus();
                        break;
                    }
                    else if (laskuri.getNykyinenHeitto() == 21)
                    {
                        laskuri.kivaTulostus();
                        break;
                    }
                    else
                    {
                        laskuri.kivaTulostus();
                        Console.WriteLine();
                    }
                }

                Console.WriteLine("Aloitetanko uusi peli? (K/E)");
                string uusiPeli = Console.ReadLine().ToUpper();
                if (uusiPeli=="E")
                {
                    break;
                } else
                {
                    laskuri.nollaa();
                }
            }
        }

    }
}
