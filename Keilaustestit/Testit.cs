﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Keilailulaskuri;

namespace Keilaustestit
{
    [TestFixture]
    public class Testit
    {
        private KeilausLaskuri laskuri = null;

        [SetUp]
        public void AlustaTestit()
        {
            laskuri = new KeilausLaskuri();
        }

        private void lisaa7ja2(int ruudut)
        {
            for (int i = 0; i < ruudut; i++)
            {
                laskuri.lisaaHeitto("7");
                laskuri.lisaaHeitto("2");
            }
        }

        [Test]
        public void LuoKeilauslaskuri()
        {
            Assert.IsInstanceOf<KeilausLaskuri>(laskuri);
        }

        [Test]
        public void LisääHeittoJaTulostaPistemaara()
        {
            laskuri.lisaaHeitto("7");

            Assert.That(0, Is.EqualTo(laskuri.pisteet()));
        }

        [Test]
        public void YksiRuutuHeitettyPisteet()
        {
            lisaa7ja2(1);

            Assert.That(9, Is.EqualTo(laskuri.pisteet()));
        }

        [Test]
        public void KokoPeliIlmanKaatoja()
        {
            lisaa7ja2(9);

            laskuri.lisaaHeitto("7");
            laskuri.lisaaHeitto("1");
            laskuri.lisaaHeitto("1");

            Assert.That(89, Is.EqualTo(laskuri.pisteet()));
        }

        [Test]
        public void YksiPaikkoJaKaksiHeittoa()
        {
            laskuri.lisaaHeitto("2");
            laskuri.lisaaHeitto("/");

            lisaa7ja2(1);

            Assert.That(26, Is.EqualTo(laskuri.pisteet()));
        }

        [Test]
        public void KaikissaRuuduissaPaikko()
        {
            laskuri.lisaaHeitto("2");
            laskuri.lisaaHeitto("/");

            laskuri.lisaaHeitto("2");
            laskuri.lisaaHeitto("/");

            laskuri.lisaaHeitto("2");
            laskuri.lisaaHeitto("/");

            laskuri.lisaaHeitto("2");
            laskuri.lisaaHeitto("/");

            laskuri.lisaaHeitto("2");
            laskuri.lisaaHeitto("/");

            laskuri.lisaaHeitto("2");
            laskuri.lisaaHeitto("/");

            laskuri.lisaaHeitto("2");
            laskuri.lisaaHeitto("/");

            laskuri.lisaaHeitto("2");
            laskuri.lisaaHeitto("/");

            laskuri.lisaaHeitto("2");
            laskuri.lisaaHeitto("/");

            laskuri.lisaaHeitto("2");
            laskuri.lisaaHeitto("/");
            laskuri.lisaaHeitto("1");

            Assert.That(119, Is.EqualTo(laskuri.pisteet()));
        }

        [Test]
        public void EkassaRuudussaPaikkoEikaSenJalkeenHeittoja()
        {
            laskuri.lisaaHeitto("7");
            laskuri.lisaaHeitto("/");

            Assert.That(0, Is.EqualTo(laskuri.pisteet()));
        }

        [Test]
        public void EkassaRuudussaKaatoJaKaksiNormiheittoa()
        {
            laskuri.lisaaHeitto("X");

            lisaa7ja2(1);

            Assert.That(28, Is.EqualTo(laskuri.pisteet()));
        }

        [Test]
        public void KaadotVikassaRuudussa()
        {
            lisaa7ja2(9);

            laskuri.lisaaHeitto("X");
            laskuri.lisaaHeitto("X");
            laskuri.lisaaHeitto("X");

            Assert.That(111, Is.EqualTo(laskuri.pisteet()));
        }

        [Test]
        public void KaatoJaSenJalkeenYksiHeitto()
        {
            lisaa7ja2(1);

            laskuri.lisaaHeitto("X");

            laskuri.lisaaHeitto("2");

            Assert.That(9, Is.EqualTo(laskuri.pisteet()));
        }

        [Test]
        public void KaadonJalkeenEiYhtaanHeittoa()
        {
            lisaa7ja2(1);

            laskuri.lisaaHeitto("X");

            Assert.That(9, Is.EqualTo(laskuri.pisteet()));
        }

        [Test]
        public void KaadonJalkeenKaksiKaatoa()
        {
            lisaa7ja2(1);

            laskuri.lisaaHeitto("X");

            laskuri.lisaaHeitto("X");

            laskuri.lisaaHeitto("X");

            Assert.That(39, Is.EqualTo(laskuri.pisteet()));
        }

        [Test]
        public void KaadonJalkeenYksiKaatoJaNormiheitto()
        {
            lisaa7ja2(1);

            laskuri.lisaaHeitto("X");

            laskuri.lisaaHeitto("X");

            laskuri.lisaaHeitto("7");

            Assert.That(36, Is.EqualTo(laskuri.pisteet()));
        }

        [Test]
        public void KaksiOhiheittoa()
        {
            laskuri.lisaaHeitto("-");
            laskuri.lisaaHeitto("-");

            Assert.That(0, Is.EqualTo(laskuri.pisteet()));
        }

        [Test]
        public void EkaHeittoOhi()
        {
            laskuri.lisaaHeitto("-");
            laskuri.lisaaHeitto("7");

            Assert.That(7, Is.EqualTo(laskuri.pisteet()));
        }

        [Test]
        public void KaksiHutia()
        {
            laskuri.lisaaHeitto("-");
            laskuri.lisaaHeitto("-");

            Assert.That(0, Is.EqualTo(laskuri.pisteet()));
        }

        [Test]
        public void OhiPaikkoJaEiKaato()
        {
            laskuri.lisaaHeitto("-");
            laskuri.lisaaHeitto("/");

            laskuri.lisaaHeitto("7");

            Assert.That(17, Is.EqualTo(laskuri.pisteet()));
        }

        [Test]
        public void OhiPaikkoJaKaato()
        {
            laskuri.lisaaHeitto("-");
            laskuri.lisaaHeitto("/");

            laskuri.lisaaHeitto("X");

            Assert.That(20, Is.EqualTo(laskuri.pisteet()));
        }

        [Test]
        public void OhiPaikkoTyhjä ()
        {
            laskuri.lisaaHeitto("-");
            laskuri.lisaaHeitto("/");

            Assert.That(0, Is.EqualTo(laskuri.pisteet()));
        }

        [Test]
        public void OhiPaikkoOhi ()
        {
            laskuri.lisaaHeitto("-");
            laskuri.lisaaHeitto("/");

            laskuri.lisaaHeitto("-");

            Assert.That(10, Is.EqualTo(laskuri.pisteet()));
        }

        [Test]
        public void PaikonJalkeenOhi()
        {
            laskuri.lisaaHeitto("7");
            laskuri.lisaaHeitto("/");

            laskuri.lisaaHeitto("-");

            Assert.That(10, Is.EqualTo(laskuri.pisteet()));
        }

        [Test]
        public void KaadonJalkeenOhiJaEiKaato()
        {
            laskuri.lisaaHeitto("X");

            laskuri.lisaaHeitto("-");
            laskuri.lisaaHeitto("7");

            Assert.That(24, Is.EqualTo(laskuri.pisteet()));
        }

        [Test]
        public void KaadonJalkeenKaatoJaOhi()
        {
            laskuri.lisaaHeitto("X");

            laskuri.lisaaHeitto("X");

            laskuri.lisaaHeitto("-");

            Assert.That(20, Is.EqualTo(laskuri.pisteet()));
        }

        [Test]
        public void KaadonJalkeenHutiJaPaikko()
        {
            laskuri.lisaaHeitto("X");

            laskuri.lisaaHeitto("-");
            laskuri.lisaaHeitto("/");

            Assert.That(20, Is.EqualTo(laskuri.pisteet()));
        }

        [Test]
        public void KaatoHutiPaikkoJaNormiheitto()
        {
            laskuri.lisaaHeitto("X");

            laskuri.lisaaHeitto("-");
            laskuri.lisaaHeitto("/");
            laskuri.lisaaHeitto("1");

            Assert.That(31, Is.EqualTo(laskuri.pisteet()));
        }

        [Test]
        public void KaikkiKaatoja()
        {
            for (int i=0; i<9; i++)
            {
                laskuri.lisaaHeitto("X");
            }

            laskuri.lisaaHeitto("X");
            laskuri.lisaaHeitto("X");
            laskuri.lisaaHeitto("X");

            Assert.That(300, Is.EqualTo(laskuri.pisteet()));
        }

        [Test]
        public void KaatoKaatoJaSeiska()
        {
            laskuri.lisaaHeitto("X");

            laskuri.lisaaHeitto("X");

            laskuri.lisaaHeitto("7");

            Assert.That(27, Is.EqualTo(laskuri.pisteet()));
        }

        [Test]
        public void KaksiKaatoa()
        {
            laskuri.lisaaHeitto("X");

            laskuri.lisaaHeitto("X");

            Assert.That(0, Is.EqualTo(laskuri.pisteet()));
        }

        [Test]
        public void VainYksiKaato()
        {
            laskuri.lisaaHeitto("X");

            Assert.That(0, Is.EqualTo(laskuri.pisteet()));
        }

        [Test]
        public void KaatoHeittoPaikko()
        {
            laskuri.lisaaHeitto("X");

            laskuri.lisaaHeitto("9");
            laskuri.lisaaHeitto("/");

            Assert.That(20, Is.EqualTo(laskuri.pisteet()));
        }

        [Test]
        public void NormiheittoJaOhi()
        {
            laskuri.lisaaHeitto("7");
            laskuri.lisaaHeitto("-");

            Assert.That(7, Is.EqualTo(laskuri.pisteet()));
        }

        [Test]
        public void HeittoPaikkoJaKaato()
        {
            laskuri.lisaaHeitto("2");
            laskuri.lisaaHeitto("/");

            laskuri.lisaaHeitto("X");

            Assert.That(20, Is.EqualTo(laskuri.pisteet()));
        }

        [Test]
        public void HeittoPaikkoJaOhi()
        {
            laskuri.lisaaHeitto("2");
            laskuri.lisaaHeitto("/");

            laskuri.lisaaHeitto("-");

            Assert.That(10, Is.EqualTo(laskuri.pisteet()));
        }

        [Test]
        public void HeittoPaikkoJaHeitto()
        {
            laskuri.lisaaHeitto("2");
            laskuri.lisaaHeitto("/");

            laskuri.lisaaHeitto("3");

            Assert.That(13, Is.EqualTo(laskuri.pisteet()));
        }

        [Test]
        public void JosVikanRuudunTokaHeittoOnOhiÄläLaskeVikaaNormiHeitto()
        {
            lisaa7ja2(9);

            laskuri.lisaaHeitto("2");
            laskuri.lisaaHeitto("-");
            laskuri.lisaaHeitto("3");

            Assert.That(83, Is.EqualTo(laskuri.pisteet()));
        }

        [Test]
        public void JosVikanRuudunTokaHeittoOnOhiÄläLaskeVikaaKaato()
        {
            lisaa7ja2(9);

            laskuri.lisaaHeitto("X");
            laskuri.lisaaHeitto("-");
            laskuri.lisaaHeitto("3");

            Assert.That(91, Is.EqualTo(laskuri.pisteet()));
        }

        [Test]
        public void VikassaRuudussaOhiheittoja()
        {
            lisaa7ja2(9);

            laskuri.lisaaHeitto("-");
            laskuri.lisaaHeitto("7");
            laskuri.lisaaHeitto("-");

            Assert.That(88, Is.EqualTo(laskuri.pisteet()));
        }

        [Test]
        public void VikassaRuudussaKaikkiOhi()
        {
            lisaa7ja2(9);

            laskuri.lisaaHeitto("-");
            laskuri.lisaaHeitto("-");
            laskuri.lisaaHeitto("-");

            Assert.That(81, Is.EqualTo(laskuri.pisteet()));
        }

        [Test]
        public void HeittojenNättiTulostus()
        {
            lisaa7ja2(3);

            //4
            laskuri.lisaaHeitto("7");
            laskuri.lisaaHeitto("/");

            //5
            laskuri.lisaaHeitto("8");
            laskuri.lisaaHeitto("1");

            //6
            laskuri.lisaaHeitto("-");
            laskuri.lisaaHeitto("-");

            //7
            laskuri.lisaaHeitto("X");

            lisaa7ja2(2);

            laskuri.lisaaHeitto("2");
            laskuri.lisaaHeitto("/");
            laskuri.lisaaHeitto("X");

            //               1       2       3       4       5       6       7       8       9         10
            Assert.That("| 7 | 2 | 7 | 2 | 7 | 2 | 7 | / | 8 | 1 | - | - | X |   | 7 | 2 | 7 | 2 | 2 | / | X |", Is.EqualTo(laskuri.tulostaHeittorivi()));
        }

        [Test]
        public void nykyisenRuudunNumero()
        {
            laskuri.lisaaHeitto("1");

            Assert.That(1, Is.EqualTo(laskuri.nykyisenRuudunNumero()));
        }

        [Test]
        public void nykyisenRuudunNumero2()
        {
            lisaa7ja2(3);

            Assert.That(4, Is.EqualTo(laskuri.nykyisenRuudunNumero()));
        }

        [Test]
        public void nykyisenRuudunNumero3()
        {
            lisaa7ja2(9);

            laskuri.lisaaHeitto("7");
            laskuri.lisaaHeitto("/");

            Assert.That(10, Is.EqualTo(laskuri.nykyisenRuudunNumero()));
        }

        [Test]
        public void nykyisenRuudunNumero4()
        {
            lisaa7ja2(9);

            laskuri.lisaaHeitto("7");
            laskuri.lisaaHeitto("/");
            laskuri.lisaaHeitto("X");

            Assert.That(10, Is.EqualTo(laskuri.nykyisenRuudunNumero()));
        }

        [Test]
        public void nykyisenRuudunHeitonNumero()
        {
            lisaa7ja2(3);

            Assert.That(1, Is.EqualTo(laskuri.nykyisenHeitonNumeroRuudussa()));
        }

        [Test]
        public void nykyisenRuudunHeitonNumero2()
        {
            lisaa7ja2(3);

            laskuri.lisaaHeitto("2");

            Assert.That(2, Is.EqualTo(laskuri.nykyisenHeitonNumeroRuudussa()));
        }

        [Test]
        public void nykyisenRuudunHeitonNumero3()
        {
            lisaa7ja2(9);

            laskuri.lisaaHeitto("2");

            Assert.That(2, Is.EqualTo(laskuri.nykyisenHeitonNumeroRuudussa()));
        }

        [Test]
        public void nykyisenRuudunHeitonNumero4()
        {
            lisaa7ja2(9);

            laskuri.lisaaHeitto("2");
            laskuri.lisaaHeitto("/");

            Assert.That(3, Is.EqualTo(laskuri.nykyisenHeitonNumeroRuudussa()));
        }
    }
}