﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Keilailulaskuri
{
    public class KeilausLaskuri
    {
        String[] heitot;
        int NykyinenHeitto;
        int[] pisteTaulukko;

        public int getNykyinenHeitto()
        {
            return NykyinenHeitto;
        }

        public KeilausLaskuri()
        {
            //Yhdessä pelihierroksessa on maksimissaan 21 heittoa.
            //9x2 heiton ruutua ja viimeinen 1x3 heiton ruutu
            heitot = new String[21];
            NykyinenHeitto = 0;
            pisteTaulukko = new int[10];
        }

        public void lisaaHeitto(string syote)
       {
            //Hoidetaan ensimmäinen ruutu, koska muuten seuraava testi
            //yrittää etsiä indeksiä -1.
            if (NykyinenHeitto==0)
            {
                heitot[0] = syote;
                if(syote=="X")
                {
                    NykyinenHeitto++;
                }
                NykyinenHeitto++;
                return;
            }
            //Jos saadaan kaato, niin skipataan seuraavaan ruutuun,
            //ellei olla viimeisessä ruudussa.
            if (syote=="X" && NykyinenHeitto < 18)
            {
                heitot[NykyinenHeitto] = syote;
                NykyinenHeitto++;
                NykyinenHeitto++;
            } //Muuten lisätään heitto ja mennän eteenpäin
            else
            {
                heitot[NykyinenHeitto] = syote;
                NykyinenHeitto++;
            }

            pisteet();
        }

        public int pisteet()
        {
            int pistemaara=0;
            //Normaalien ruutujen pistelasku
            for (int i=0; i<18; i+=2)
            { 
               //Tarkistetaan kaatotilanteet ensin
               if (heitot[i]=="X")
               {
                    //Seuraava heitto on kaato
                    if (heitot[i+2]=="X")
                    {
                        //Ja sitä seuraava heitto on kaato 
                        if (heitot[i+4] == "X")
                        {
                            //Lisätään pisteisiin 30
                            pistemaara += 30;
                            //ja skipataan seuraavaan ruutuun
                        }
                        //Jos sitä seuraava heitto on ohiheitto
                        else if (heitot[i+4] == "-")
                        {
                            //Lisätään pisteisiin 20
                            pistemaara += 20;
                            //ja skipataan seuraavaan ruutuun
                        }
                        //Jos toista heittoa ei ole heitetty
                        else if (heitot[i+4]==null)
                        {
                            //Mennään pois, sillä tämän ruudun pistemäärää
                            //(eikä täten seuraavienkaan) voida laskea
                            return pistemaara;
                        }
                        //Muutoin lisätään heiton pistemäärä
                        else
                        {
                            pistemaara += 20 + int.Parse(heitot[i + 4]);
                            //Ja skipataan seuraavaan ruutuun
                        }
                    }
                    //Jos taas kaatoa seuraava heitto onkin tyhjä
                    else if (heitot[i+2]==null)
                    {
                        //Mennään pois, sillä tämän ruudun pistemäärää
                        //(eikä täten seuraavienkaan) voida laskea
                        return pistemaara;
                    }
                    //Jos taas kaatoa seuraa ohiheitto
                    else if (heitot[i+2]=="-")
                    {
                        //Ja sitä seuraava heitto on paikko
                        if (heitot[i+3]=="/")
                        {
                            //Kaadosta 10 pistettä ja seuraavasta ruudusta 10
                            pistemaara += 20;
                        }
                        //Ja sitä seuraava heitto on tyhjä
                        else if (heitot[i+3]==null)
                        {
                            //Mennään pois, sillä tämän ruudun pistemäärää
                            //(eikä täten seuraavienkaan) voida laskea
                            return pistemaara;
                        }
                        //Ja sitä seuraa toinen huti
                        else if (heitot[i+3]=="-")
                        {
                            pistemaara += 10;
                        }
                        //Tavallinen heitto
                        else
                        {
                            pistemaara += 10 + int.Parse(heitot[i+3]);
                        }
                    }
                    //jos kaatoa seuraa tavallinen heitto
                    else {
                        //Ja sitä seuraava heitto on paikko
                        if (heitot[i + 3] == "/")
                        {
                            //Kaadosta 10 pistettä ja seuraavasta ruudusta 10
                            pistemaara += 20;
                        }
                        //Ja sitä seuraava heitto on tyhjä
                        else if (heitot[i + 3] == null)
                        {
                            //Mennään pois, sillä tämän ruudun pistemäärää
                            //(eikä täten seuraavienkaan) voida laskea
                            return pistemaara;
                        }
                        //Ja sitä seuraa huti
                        else if (heitot[i + 3] == "-")
                        {
                            pistemaara += 10 + int.Parse(heitot[i+2]);
                        }
                        //Tavallinen heitto
                        else
                        {
                            pistemaara += 10 + int.Parse(heitot[i + 2]) + int.Parse(heitot[i+3]);
                        }
                    }
                }
               //Jos eka heitto taas meni ohi
               else if (heitot[i] == "-")
               {
                    //Ja seuraava heitto on tyhjä
                    if (heitot[i+1]==null)
                    {
                        return pistemaara;
                    }
                    //Ja seuraava heitto on paikko
                    else if (heitot[i+1]=="/")
                    {
                        //Jos sitä seuraava on tyhjä
                        if (heitot[i+2]==null)
                        {
                            return pistemaara;
                        }
                        //Jos sitä seuraava on kaato
                        else if (heitot[i+2]=="X")
                        {
                            pistemaara += 20;                       
                        }
                        //Jos seuraava on ohi
                        else if (heitot[i+2]=="-")
                        {
                            pistemaara += 10;
                        }
                        //Jos seuraava on normiheitto
                        else
                        {
                            pistemaara += 10 + int.Parse(heitot[i + 2]);
                        }
                    }
                    //Jos toinen heittokin meni ohi
                    else if (heitot[i+1]=="-")
                    {
                        //Ei tehdä mitään
                    }
                    //Jos taas oli normiheitto
                    else
                    {
                        pistemaara += int.Parse(heitot[i + 1]);
                    }
               }
               //Jos ruudun eka heitto on tyhjä, niin ollaan saatu laskettua loppuun asti
               else if (heitot[i]==null)
               {
                    return pistemaara;
               }
               //Jos eka heitto onkin ihan tavallinen heitto
               else
                {
                    //Jos seuraava heitto on tyhjä
                    if (heitot[i+1]==null)
                    {
                        //Ollaan lopussa ja voidaan lähteä pois
                        return pistemaara;
                    }
                    //Jos toka heitto meni ohi
                    else if (heitot[i+1]=="-")
                    {
                        pistemaara += int.Parse(heitot[i]);
                    }
                    //Jos tuli paikko
                    else if (heitot[i + 1] == "/")
                    {
                        //Jos sitä seuraava on tyhjä
                        if (heitot[i + 2] == null)
                        {
                            return pistemaara;
                        }
                        //Jos sitä seuraava on kaato
                        else if (heitot[i + 2] == "X")
                        {
                            pistemaara += 20;
                        }
                        //Jos seuraava on ohi
                        else if (heitot[i + 2] == "-")
                        {
                            pistemaara += 10;
                        }
                        //Jos seuraava on normiheitto
                        else
                        {
                            pistemaara += 10 + int.Parse(heitot[i + 2]);
                        }
                    }
                    //Jos tokakin heitto on ihan tavallinen
                    else
                    {
                        pistemaara += int.Parse(heitot[i]) + int.Parse(heitot[i + 1]);
                    }
                }

                //Tänne tullaan vasta, kun ruutu ollaan saatu laskettua
                pisteTaulukko[i / 2] = pistemaara;

            }

            //Vikan ruudun lasku jos saa heittää kaikki heitot
            if (heitot[18] != null && heitot[19] != null)
            {
                int lasketaanAsti=0;
                if ((heitot[19]=="X" || heitot[19]=="/") && heitot[20]!=null)
                {
                    lasketaanAsti = 21;
                }
                else if (heitot[19] != "X" || heitot[19] != "/")
                {
                    lasketaanAsti = 20;
                }

                for (int i = 18; i<lasketaanAsti; i++)
                {
                    if (heitot[i]=="/")
                    {
                        pistemaara += 10 - int.Parse(heitot[i - 1]);
                    } else if (heitot[i]=="X")
                    {
                        pistemaara += 10;
                    } else if (heitot[i]== "-")
                    {
                        //Ei tehdä mitään
                    }
                        else
                    {
                        pistemaara += int.Parse(heitot[i]);
                    }
                }

                pisteTaulukko[9] = pistemaara;
            }
 
            return pistemaara;
        }

        public void kivaTulostus()
        {
            Console.WriteLine("+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+");
            Console.WriteLine(tulostaHeittorivi());
            Console.WriteLine("+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+");
            Console.WriteLine(tulostaPisterivi());
            Console.WriteLine("+-------+-------+-------+-------+-------+-------+-------+-------+-------+-----------+");
        }

        public string tulostaHeittorivi()
        {
            string tulostus;

            //lisätään alkuun pystyviiva
            tulostus = "|";
            for (int i = 0; i < 21; i++)
            {
                tulostus += " ";
                //Jos heitto on tyhjä, niin lisätään siihen pystyviiva
                if (heitot[i]==null)
                {
                    tulostus += " ";
                }
                //Muutoin lisätään siihen heitto
                else
                {
                    tulostus += heitot[i];
                }

                //Ja loppuun vielä välilyönti ja pystyviiva
                tulostus += " |";
            }

            return tulostus;
        }

        public string tulostaPisterivi()
        {
            string tulostus;
            //lisätään alkuun pystyviiva
            tulostus = "|";

            //Lisätään ruutujen 1-9 pistemäärä
            for (int i=0; i<9; i++)
            {
                string pistemaara1 = pisteTaulukko[i].ToString();

                if (pistemaara1=="0")
                {
                    tulostus += "       |";
                }
                else if(pistemaara1.Length == 1)
                {
                    tulostus += "   ";
                    tulostus += pistemaara1;
                    tulostus += "   |";
                }
                else if (pistemaara1.Length == 2)
                {
                    tulostus += "  ";
                    tulostus += pistemaara1;
                    tulostus += "   |";
                }
                else if (pistemaara1.Length==3)
                {
                    tulostus += "  ";
                    tulostus += pistemaara1;
                    tulostus += "  |";
                }
            }

            //Lisätään ruudun 10 pistemäärä

            string pistemaara2 = pisteTaulukko[9].ToString();
            if (pistemaara2=="0")
            {
                tulostus += "           |";
            }
            else if (pistemaara2.Length == 1)
            {
                tulostus += "     ";
                tulostus += pistemaara2;
                tulostus += "     |";
            }
            else if (pistemaara2.Length == 2)
            {
                tulostus += "    ";
                tulostus += pistemaara2;
                tulostus += "     |";
            }
            else if (pistemaara2.Length == 3)
            {
                tulostus += "    ";
                tulostus += pistemaara2;
                tulostus += "    |";
            }

            return tulostus;
        }

        public int nykyisenRuudunNumero()
        {
            if (NykyinenHeitto > 17)
            {
                return 10;
            } else
            {
                int heittonumero = NykyinenHeitto + 2;
                return heittonumero / 2;
            }
        }

        public int nykyisenHeitonNumeroRuudussa()
        {
            if (NykyinenHeitto > 17)
            {
                return NykyinenHeitto - 17;
            }
            else
            {
                return (NykyinenHeitto % 2) + 1;
            }
        }

        public void nollaa()
        {
            heitot = new String[21];
            NykyinenHeitto = 0;
            pisteTaulukko = new int[10];
        }
    }
}